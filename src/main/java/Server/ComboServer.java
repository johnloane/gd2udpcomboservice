package Server;

/*
    Combo Service Protocol Descriptor
    Host: 127.0.0.1/localhost
    Port: 50000
    Communication method: UDP
    Encoding: Text

    Message Format:
    Echo:
    Client request: echo&&$message
    Server response: $message

    Daytime:
    Client request: daytime
    Server response: $dateInfo

    Statistics:
    Client request: stats
    Server response: $NumberOfRequestsHandled

    Server response to all other commands: Unrecognised request

    Sequence diagram
    Client-----------------------------Server
          ------echo&&hey there------->
          <-----hey there--------------
          ------daytime--------------->
          <-----Tuesday March 26 10:43-
          ------stats----------------->
          <-----3----------------------
          ------banana---------------->
          <-----Unrecognised Request--
 */

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;

public class ComboServer {
    public static void main(String[] args) {
        //Set up breaking character info
        String breakingCharacters = "&&";

        //Set up the max acceptable payload
        //1300 is associated with MTU at link layer
        final int MAXPAYLOAD = 1300;

        //Variable to track number of requests
        int numberOfRequests = 0;
        boolean continueRunning = true;

        DatagramSocket serverSocket = null;

        try{
            //Create a socket to listen for messages and receive
            //messages
            serverSocket = new DatagramSocket(ComboServiceDetails.SERVERPORT);
            System.out.println("Listening on port: " + ComboServiceDetails.SERVERPORT);

            //Loop forever processing requests
            while(continueRunning){
                //Create space to read the data into
                byte[] incomingMessageBuffer = new byte[MAXPAYLOAD];
                DatagramPacket incomingPacket = new DatagramPacket(incomingMessageBuffer, incomingMessageBuffer.length);
                //wait to receive into the packet
                //note that receive is blocking
                serverSocket.receive(incomingPacket);
                //Get data out of packet
                String data = new String(incomingPacket.getData());

                //Process the data
                data = data.trim();

                //Tokenise the message
                String[] components = data.split(breakingCharacters);
                numberOfRequests++;

                String response = null;

                //Work out what command has been sent
                //If the first component is echo
                if(components[0].equalsIgnoreCase("echo")){
                    //Strip out the message and put into response
                    //response = components[1];
                    response = data.replace("echo"+breakingCharacters, "");
                }
                else if(components[0].equalsIgnoreCase("daytime")){
                    response = new Date().toString();
                }
                else if(components[0].equalsIgnoreCase("stats")){
                    response = "The number of requests dealt with is " + numberOfRequests;
                }
                else{
                    response = "Unrecognised request";
                }
                //Get the address of the requestor
                InetAddress clientAddress = incomingPacket.getAddress();

                byte[] responseBuffer = response.getBytes();
                //Create a packet to store the response
                DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length, clientAddress, ComboServiceDetails.CLIENTPORT);
                serverSocket.send(responsePacket);
            }
        }catch(IOException iox){
            iox.printStackTrace();
        }finally{
            if(serverSocket != null){
                serverSocket.close();
            }
        }
    }
}
