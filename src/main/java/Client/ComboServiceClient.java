package Client;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboServiceClient {

    public static void main(String[] args) {
        //Set up a scanner to get input from the user
        Scanner input = new Scanner(System.in);

        //Set up the socket object so it can be closed in the finally
        DatagramSocket clientSocket = null;

        try{
            //Set up the server address information
            InetAddress serverAddress = InetAddress.getByName("localhost");
            //Bind socket to a port
            clientSocket = new DatagramSocket(ComboServiceDetails.CLIENTPORT);

            boolean continueRunning = true;

            while(continueRunning){
                displayMenu();
                int choice  = getChoice(input);

                String message = null;
                boolean sendMessage = true;

                switch(choice){
                    case -1:{
                        continueRunning = false;
                        sendMessage = false;
                        break;
                    }
                    case 1:{
                        //Clear the buffer before getting the message to send
                        input.nextLine();
                        System.out.println("Please enter the message to echo: ");
                        message = "echo"+ComboServiceDetails.breakingCharacters+input.nextLine();
                        break;
                    }
                    case 2:{
                        message = "daytime";
                        break;
                    }
                    case 3:{
                        message = "stats";
                        break;
                    }
                    default:{
                        System.out.println("Bad request. Please choose from the options listed.");
                        sendMessage = false;
                    }
                }

                //If the sendMessage flag is true the user choose a valid option
                if(sendMessage){
                    //Get the information to be sent as a byte array, can't send strings through sockets directly
                    byte buffer[] = message.getBytes();
                    DatagramPacket clientPacket = new DatagramPacket(buffer, buffer.length, serverAddress, ComboServiceDetails.SERVERPORT);

                    //Send the message
                    clientSocket.send(clientPacket);
                    System.out.println("Message sent");

                    //Deal with the response
                    byte[] clientBuffer = new byte[ComboServiceDetails.MAXPAYLOAD];
                    DatagramPacket receivePacket = new DatagramPacket(clientBuffer, clientBuffer.length);
                    clientSocket.receive(receivePacket);

                    //Get the data out of the packet
                    String data = new String(receivePacket.getData());
                    System.out.println("Response: " + data.trim() + ".");
                }
            }
            System.out.println("Thank you for using the service");
            Thread.sleep(3000);
        }catch(UnknownHostException uex){
            uex.printStackTrace();
        }catch(SocketException se){
            se.printStackTrace();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        catch(IOException iox){
            iox.printStackTrace();
        }finally{
            if(clientSocket != null){
                clientSocket.close();
            }
        }
    }

    public static void displayMenu(){
        System.out.println("Please choose one of the following options: ");
        System.out.println("1) Echo a message");
        System.out.println("2) Get current date and time");
        System.out.println("3) View number of requests made to the server");
        System.out.println("-1) To exit the program");
    }

    public static int getChoice(Scanner input){
        int choice = 0;
        boolean validNumber = false;
        while(!validNumber){
            try{
                choice = input.nextInt();
                validNumber = true;
            }catch(InputMismatchException e){
                System.out.println("Please enter a number from the menu");
                System.out.println();
                //Clear the scanner's buffer
                input.nextLine();
                displayMenu();
            }
        }
        return choice;
    }
}
