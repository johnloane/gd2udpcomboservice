package Core;

public class ComboServiceDetails {
    public static final int SERVERPORT = 50000;
    public static final int CLIENTPORT = 50001;
    public static final String breakingCharacters = "&&";
    public static final int MAXPAYLOAD = 1300;
}
